package Pessoas;

import main.java.cest.edu.Endereco;

public class PessoaFisica implements Pessoa {
	private String nome;
    private String cpf;
    private Endereco endereco;
	

    public PessoaFisica(String nome, String cpf) {
        this.nome = nome;
        this.cpf = cpf;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    @Override
    public String getNome() {
        return this.nome;
    }

    @Override
    public String getEndereco() {
        return this.endereco.toString();
    }


    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

	@Override
	public String getCPF() {
		// TODO Auto-generated method stub
		return null;
	}
    
}

	
	
	

