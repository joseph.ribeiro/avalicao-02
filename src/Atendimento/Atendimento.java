package Atendimento;

import java.util.Date;

public class Atendimento {
	public Date Data;
	public Integer Numero;
	
	
	public Date getData() {
		return Data;
	}
	public void setData(Date data) {
		this.Data = data;
	}
	public Integer getNumero() {
		return Numero;
	}
	public void setNumero(Integer numero) {
		this.Numero = numero;
	}
	
	 public Atendimento(Date data, Integer numero) {
	        this.Data = data;
	        this.Numero = numero;
	    }
	 
}
	    
	   